package ru.iimm.utils.taskchecker;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class checkerTest {

    public void initialResultFileCreationTest() throws IOException {
        // === Form initial result file ===
        String resultDir = "./res";
        String exension = ".exe";
        String resultFilePath = resultDir + "_res.csv";
        String checkedResultFilePath = resultDir + "_res_chk.csv";


        FileManager fileManager = new FileManager();
        InitialResultListCreator initialResultListCreator = new InitialResultListCreator();

        List<Path> unitFilesPaths = fileManager.getFilePaths(resultDir);
        fileManager.writeResultFile(resultFilePath,
                initialResultListCreator.formInitialResultList(unitFilesPaths) );

    }


}