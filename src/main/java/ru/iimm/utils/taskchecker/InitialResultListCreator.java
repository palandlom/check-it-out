package ru.iimm.utils.taskchecker;

import ru.iimm.utils.taskchecker.models.ResultRowDto;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class InitialResultListCreator {

    public static final String AUTHOR_DELIMITER = "_";
//    public static final String RESULT_FILE_EXTENSION = "exe";

    private List<ResultRowDto> resultRows;

    private FileManager fileManager;

    public InitialResultListCreator() {
        this.resultRows = new ArrayList<ResultRowDto>();
        this.fileManager = new FileManager();
    }

    public List<ResultRowDto> formInitialResultList(List<Path> unitFilesPaths)
    {
        return unitFilesPaths.stream().map(path -> {
            FileTypes scriptFileType = FileTypes.getFileTypeByFilePath(path);

            ResultRowDto row = new  ResultRowDto();
            row.setOutputValue("");
            row.setInputValue("");
            row.setCorrectOutputValue("");
            row.setTaskId(getTaskIdFrom(path).orElse("None"));
            row.setAuthorId(getAuthorIdFrom(path, scriptFileType.ext()).orElse(""));
            row.setResult("");
            row.setUnitFilePath(path.toAbsolutePath().normalize().toString());
            row.setFileExtension(FileTypes.getFileTypeByFilePath(path).ext());
            row.setLanguage("");
            row.setVersionIndex("");
            return row;
        }).collect(Collectors.toList());
    }

    public static Optional<String> getAuthorIdFrom(Path filepath, String fileExtension){
        String fn = filepath.getFileName().toString();
        int startIndex = fn.lastIndexOf(AUTHOR_DELIMITER);
        int endIndex = fn.lastIndexOf(fileExtension);
        return startIndex>0 && endIndex>0 ? Optional.of(fn.substring(startIndex+1, endIndex-1)) : Optional.empty();
    }

    public static Optional<String> getTaskIdFrom(Path filepath){
        String fn = filepath.getFileName().toString();
        int endIndex = fn.indexOf(AUTHOR_DELIMITER);
        return  endIndex>0 ? Optional.of(fn.substring(0, endIndex)) : Optional.empty();
    }


}
