//package ru.iimm.utils.taskchecker;
//
//import io.swagger.client.ApiClient;
//import io.swagger.client.ApiException;
//import io.swagger.client.ApiResponse;
//import io.swagger.client.api.CreditSpentApi;
//import io.swagger.client.api.ExecuteApi;
//import io.swagger.client.model.Credit;
//import io.swagger.client.model.Execute;
//import io.swagger.client.model.InlineResponse200;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import ru.iimm.utils.taskchecker.execution.AbstractExecutor;
//import ru.iimm.utils.taskchecker.execution.jdoodle.JdoodleExecutor;
//import ru.iimm.utils.taskchecker.models.KeyRowDto;
//import ru.iimm.utils.taskchecker.models.ResultRowDto;
//
//import java.io.IOException;
//import java.nio.file.Path;
//import java.util.ArrayList;
//import java.util.List;
//@Deprecated // ontly to check real work from ./scripts_real
//class IntegralRealTest {
//    public static final String RESULT_DIR_PATH = "./scripts_real";
//    public static final String REPORT_FILE_PATH = RESULT_DIR_PATH+"/report_all.csv";
//    public static final String KEYS_FILE_PATH = RESULT_DIR_PATH+"/keys.csv";
//    public static final String ERRORS_FILE_PATH=RESULT_DIR_PATH+"/errors.log";
//
//    private static final Logger log = LoggerFactory.getLogger( IntegralRealTest.class );
//
//
//
//    @Test
//    void integralTest() throws IOException {
//        // ==========================
//        // === check getFilesPath ===
//        // ==========================
//        FileManager fileManager = new FileManager();
//        List<Path> filesPaths = fileManager.getFilePaths(RESULT_DIR_PATH);
//
//        // ================================
//        // === check save/read key file ===
//        // ================================
////        List<KeyRowDto> keys = new ArrayList<>();
////        keys.add(new KeyRowDto("1","1", "1"));
////        keys.add(new KeyRowDto("1","a a a", "a a a"));
////        keys.add(new KeyRowDto("2","b b b", "b b b"));
////        keys.add(new KeyRowDto("2","1 2 3", "1 2 3"));
////        keys.add(new KeyRowDto("3","c c c", "c c c"));
////        keys.add(new KeyRowDto("1","in", "out"));
////        keys.add(new KeyRowDto("2","in", "out"));
////        keys.add(new KeyRowDto("3","in", "out"));
//
////        fileManager.writeKeysFile(KEYS_FILE_PATH, keys);
//        List<KeyRowDto> keys = fileManager.readKeysFile(KEYS_FILE_PATH);
//
//        // ========================================
//        // === check generation of empty report ===
//        // ========================================
//        InitialResultListCreator initialResultListCreator = new InitialResultListCreator();
//        List<ResultRowDto> rows = initialResultListCreator.formInitialResultList(filesPaths);
//        rows.stream().forEach(r -> log.info(r.toString()));
//
//        // =============================
//        // === Get and check output  ===
//        // =============================
//        String clientId = "75724477ff44b628b86ca0397bbba7bc";
//        String clientSecret = "f15f1a667ffec71f313bd03cd2f72eab3007dd4e9d0993a07bdc4baeed59e823";
////        clientId = "e685250858106a8f96b4e6fa464449e1";
////        clientSecret= "c656454786a96c85143247033560b67551d582159177492f4613937b5e0e60b7";
//
////        String language = "python3";
////        String versionIndex = "0";
//
//        AbstractExecutor executor = new JdoodleExecutor(clientId, clientSecret);
//        Checker checker = new Checker(executor);
//        rows = checker.executeScriptsCheckResultAndFormFinalResultRows(keys,rows);
//
//        // ==========================
//        // === check write report ===
//        // ==========================
//        fileManager.writeResultFile(REPORT_FILE_PATH,rows);
////        Assertions.assertEquals(7, fileManager.readResultFile(REPORT_FILE_PATH).size());
////        Assertions.assertEquals(rows, fileManager.readResultFile(REPORT_FILE_PATH));
//
//        fileManager.writeErrorFile(checker.getErrors(), ERRORS_FILE_PATH);
//    }
//
//
//    @Test
//    void getFileAndFormInitialResultPaths() throws IOException {
//        // === check getFilesPath ===
//        FileManager fileManager = new FileManager();
//        List<Path> filesPaths = fileManager.getFilePaths(RESULT_DIR_PATH);
////        filesPaths.forEach(p -> log.info("Get file path from [%s]: %s",RESULT_DIR_PATH, p.toString()));
//        Assertions.assertEquals(4,filesPaths.size());
//
//        // === check read key file ===
//        List<KeyRowDto> keys = new ArrayList<>();
//        keys.add(new KeyRowDto("1","in", "out"));
//        keys.add(new KeyRowDto("2","in", "out"));
//        keys.add(new KeyRowDto("3","in", "out"));
//        fileManager.writeKeysFile(KEYS_FILE_PATH, keys);
//        keys = fileManager.readKeysFile(KEYS_FILE_PATH);
//
//        // === check generation of empty report ===
//        InitialResultListCreator initialResultListCreator = new InitialResultListCreator();
//        List<ResultRowDto> rows = initialResultListCreator.formInitialResultList(filesPaths);
//        rows.stream().forEach(r -> log.info(r.toString()));
//        Assertions.assertEquals(4, rows.size());
//
//        // === Fill report ===
//        String clientId = "75724477ff44b628b86ca0397bbba7bc";
//        String clientSecret = "f15f1a667ffec71f313bd03cd2f72eab3007dd4e9d0993a07bdc4baeed59e823";
//        String language = "python3";
//        String versionIndex = "0";
//
//        // ==========================
//        // === check write report ===
//        fileManager.writeResultFile(REPORT_FILE_PATH,rows);
//        Assertions.assertEquals(4, fileManager.readResultFile(REPORT_FILE_PATH).size());
//        Assertions.assertEquals(rows, fileManager.readResultFile(REPORT_FILE_PATH));
//
//
//
//    }
//
//    @Test
//    public void apiTest(){
//        String clientId = "e685250858106a8f96b4e6fa464449e1"; //Replace with your client ID
//
//        String clientSecret = "c656454786a96c85143247033560b67551d582159177492f4613937b5e0e60b7"; //Replace with your client Secret
//
//        clientId = "75724477ff44b628b86ca0397bbba7bc";
//        clientSecret = "f15f1a667ffec71f313bd03cd2f72eab3007dd4e9d0993a07bdc4baeed59e823";
//        String script = "";
//        String language = "java";
//        String versionIndex = "0";
//
//
//        CreditSpentApi creditSpentApi = new CreditSpentApi();
//        ApiClient client = new ApiClient();
//        client.setBasePath("https://api.jdoodle.com");
//        creditSpentApi.setApiClient(client);
//
//        Credit credit = new Credit(); // Credit | the body
//        credit.setClientId(clientId);
//        credit.setClientSecret(clientSecret);
//
//        ExecuteApi executeApi = new ExecuteApi();
//        executeApi.setApiClient(client);
//
//        try {
//            ApiResponse response = creditSpentApi.creditSpentPostWithHttpInfo(credit);
//            log.info("Response {}", response.getData());
//
//            Execute execute = new Execute();
//            execute.setClientId(clientId);
//            execute.setClientSecret(clientSecret);
//            execute.setLanguage("python3");
//            execute.setScript("value = input(\"Please enter a string:\\n\")\n" +
//                    " \n" +
//                    "print(\"You entered\", {value})");
//            execute.setStdin("pytho3-OK");
//            execute.setVersionIndex("0");
//
//            InlineResponse200 response2  = executeApi.executePost(execute);
//            log.info("execute: {}", response2.toString());
//
//
//            execute.setClientId(clientId);
//            execute.setClientSecret(clientSecret);
//
//            execute.setLanguage("java");
//            execute.setScript("    import java.io.BufferedReader;\n" +
//                    "    import java.io.IOException;\n" +
//                    "    import java.io.InputStreamReader;\n" +
//                    "    public class Test\n" +
//                    "    {\n" +
//                    "        public static void main(String[] args) throws IOException\n" +
//                    "        {\n" +
//                    "            BufferedReader reader =\n" +
//                    "                       new BufferedReader(new InputStreamReader(System.in));\n" +
//                    "            String name = reader.readLine();\n" +
//                    "            System.out.println(name);       \n" +
//                    "        }\n" +
//                    "    }");
//            execute.setStdin("java-OK");
//            execute.setVersionIndex("1");
//
//            response2  = executeApi.executePost(execute);
//            log.info("execute: {}", response2.toString());
//
//        } catch (ApiException e) {
//            System.err.println("Exception when calling CreditSpentApi#creditSpentPost");
//            e.printStackTrace();
//        }
//    }
//
//    private String getScriptOnJava(){
//        return "    import java.io.BufferedReader;\n" +
//                "    import java.io.IOException;\n" +
//                "    import java.io.InputStreamReader;\n" +
//                "    public class Test\n" +
//                "    {\n" +
//                "        public static void main(String[] args) throws IOException\n" +
//                "        {\n" +
//                "            BufferedReader reader =\n" +
//                "                       new BufferedReader(new InputStreamReader(System.in));\n" +
//                "            String name = reader.readLine();\n" +
//                "            System.out.println(name);       \n" +
//                "        }\n" +
//                "    }";
//    }
//
//    private String getScriptOnPython(){
//        return "value = input() \n" +
//                "print('{}'.format(value))\n";
//    }
//
//    private String getScriptOnCpp(){
//        return "#include <iostream>\n" +
//                "using namespace std;\n" +
//                "\n" +
//                "int main ()\n" +
//                "{\n" +
//                "    char input[100];\n" +
//                "    cin.getline(input,sizeof(input));\n" +
//                "  cout << input;\n" +
//                "  return 0;\n" +
//                "}";
//    }
//
//    private String getScriptOnCSharp(){
//        return "using System;\n" +
//                "using System.Collections.Generic;\n" +
//                "\n" +
//                "class Demo {\n" +
//                "   static void Main() {\n" +
//                "      string val;\n" +
//                "      val = Console.ReadLine();\n" +
//                "      Console.WriteLine(val);\n" +
//                "   }\n" +
//                "}";}
//
//
//}