package ru.iimm.utils.taskchecker;


import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iimm.utils.taskchecker.execution.AbstractExecutor;
import ru.iimm.utils.taskchecker.models.ErrorDto;
import ru.iimm.utils.taskchecker.models.KeyRowDto;
import ru.iimm.utils.taskchecker.models.ResultRowDto;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class Checker {

    public static final String RESULT_DELIMITER = " ";
    public static final String RESULT_OK = "1";
    public static final String RESULT_BAD = "0";
    private static final Logger log = LoggerFactory.getLogger(Checker.class);
    private AbstractExecutor executor;

    private List<ErrorDto> errors = new ArrayList<>();

    public Checker( AbstractExecutor executor) {
        this.executor = executor;
    }

    public List<ResultRowDto> executeScriptsCheckResultAndFormFinalResultRows(List<KeyRowDto> keys, List<ResultRowDto> initialResultList) {
        errors.clear();
        return keys.stream().map(keyRow -> initialResultList.stream()
                .filter(resRow -> resRow.getTaskId().equals(keyRow.getTaskId()))
                .map(resRow -> {
                    ResultRowDto finalResRow = new ResultRowDto(resRow);
                    finalResRow.setInputValue(keyRow.getInputValue());
//                        finalResRow.setInputValue(keyRow.getInputValue().replace("\\n", System.lineSeparator()));
                    finalResRow.setCorrectOutputValue(keyRow.getCorrectOutputValue());
                    // === read script, get output...
                    Optional<String> script = getScript(new File(finalResRow.getUnitFilePath()));
                    Optional<String> output = script.isPresent() ?
                            executor.execute(script.get(),
                                    finalResRow.getInputValue(),
                                    finalResRow.getFileExtension(),
                                    "0")
                            : Optional.empty();

                    // ... and check result
                    if (output.isPresent() && !isErrorOutput(output.get())) {
                        List<String> correctResults = Arrays.asList(finalResRow.getCorrectOutputValue().split(RESULT_DELIMITER));
                        List<String> outputResults = Arrays.asList(getFirstString(output.get()).split(RESULT_DELIMITER));
                        Boolean isCorrectOutput = compareList(correctResults,outputResults);
                        log.info("=== Output is correct: [{}], key = [{}] ",isCorrectOutput, finalResRow.getCorrectOutputValue());
                        finalResRow.setOutputValue(output.get());
                        finalResRow.setResult(isCorrectOutput.toString());
                    } else {
                        ErrorDto error = new ErrorDto();
                        error.setAuthorId(finalResRow.getAuthorId());
                        error.setMessage(output.orElse("EMPTY"));
                        error.setTaskId(finalResRow.getTaskId());
                        error.setUnitFilePath(finalResRow.getUnitFilePath());
                        error.setInputValue(finalResRow.getInputValue());
                        finalResRow.setResult(String.format("Error ID:%d",error.hashCode()));
                        errors.add(error);
                    }
                    return finalResRow;
                }).collect(Collectors.toList())).flatMap(resRows -> resRows.stream()).collect(Collectors.toList());
    }

    private Optional<String> getScript(File scriptFile) {
        try {
            if (scriptFile != null && scriptFile.exists() && scriptFile.isFile()) {
                return Optional.of(Utils.readFileToString(scriptFile));
            } else {
                throw new IOException();
            }
        } catch (IOException e) {
            log.error(String.format("Can't read script from [%s] - skip it",scriptFile));
            return Optional.empty();
        }
    }

    @Deprecated //not used
    private ResultRowDto getRowWithResult(ResultRowDto row) {
        List<String> results = executeUnitAndGetResults(row.getUnitFilePath(), row.getInputValue());
        List<String> correctResults = splitOutputToStrings(row.getCorrectOutputValue(), RESULT_DELIMITER);
        row.setOutputValue(String.join(RESULT_DELIMITER, results));
//        row.setResu(String.join(RESULT_DELIMITER,results));
        row.setResult(isCorrectResult(correctResults, results) ? RESULT_OK : RESULT_BAD);
        return row;
    }

    @Deprecated //not used
    private List<String> executeUnitAndGetResults(String unitFilePath, String inputString) {
        String[] cmds = {
                unitFilePath,
        };

        try {
            ProcessBuilder pb = new ProcessBuilder(cmds);
            pb.directory(Paths.get(unitFilePath).getParent().toFile());
            pb.redirectErrorStream(true);

            Process process = pb.start();

            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is,
                    StandardCharsets.UTF_8);
            BufferedReader br = new BufferedReader(isr);
            String res;
            if ((res = br.readLine()) != null) {
                System.err.println(String.format("Unit output - [%s]", res));
            }
        } catch (IOException e) {
            log.error("Error during launch of lspl: {}");
            e.printStackTrace();
            return null;
        }

        return Arrays.asList("0", "5", "7");
    }


    private boolean isCorrectResult(List<String> correctResults, List<String> results) {
        return compareList(correctResults, results);
    }

    private boolean compareList(List<String> correctResults, List<String> results) {
        Optional<List<Number>> correctNumResult = stringsToNumbers(correctResults);
        Optional<List<Number>> numResult = stringsToNumbers(results);

        if (correctNumResult.isPresent() && numResult.isPresent()) {
            return correctNumResult.get().equals(numResult.get());
        } else {
            return trimStrings(correctResults).equals(trimStrings(results));
        }
    }

    private List<String> trimStrings(List<String> strings)
    {
        return strings.stream().map(String::trim).collect(Collectors.toList());
    }

    private String getFirstString(String multiLinesString)
    {
        List<String> strings = splitMultilineString(multiLinesString);
        return strings.size() > 0 ? strings.get(0).trim() : multiLinesString;
    }

    private List<String> splitMultilineString(String multiLinesString){
        return splitOutputToStrings(multiLinesString, System.lineSeparator());
    }

    private List<String> splitOutputToStrings(String stringWithLineSeparator, String s) {
        return Arrays.asList(stringWithLineSeparator.split(s));
    }


    private Optional<List<Number>> stringsToNumbers(List<String> strings) {//NumberFormatException
        List<Number> numbers = new ArrayList<>();
        for (String str : strings) {
            try {
                Double num = Precision.round(Double.parseDouble(str), 3);
                numbers.add(num);
            } catch (NumberFormatException e) {
                numbers.clear();
                break;
            }
        }// if conversion failed return empty result
        if (numbers.size() == strings.size()) return Optional.of(numbers);
        for (String str : strings) {
            try {
                numbers.add(Long.parseLong(str));
            } catch (NumberFormatException e) {
                return Optional.empty();
            }
        }
        return Optional.of(numbers);
    }
    private Boolean isErrorOutput(String output){
        if (output.toLowerCase().contains("error")){
            return true;
        }
        else {
            return false;
        }
    }

    public List<ErrorDto> getErrors() {
        return errors;
    }
}
