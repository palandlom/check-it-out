package ru.iimm.utils.taskchecker;

import org.apache.commons.io.FilenameUtils;

import java.nio.file.Path;

public enum FileTypes {
    EXE(Extensions.EXE),
    JAR(Extensions.JAR),
    JAVA(Extensions.JAVA),
    CPP(Extensions.CPP),
    PYTHON(Extensions.PY),
    JAVASCRIPT(Extensions.JS),
    CSHARP(Extensions.CS),
    PASCAL(Extensions.JS),
    UNKNOWN("");

    FileTypes(String extension) {
        this.extension = extension;
    }

    private String extension;

    public String ext() {
        return extension;
    }

    public String fileSuffix() {
        return "."+extension;
    }


    public static FileTypes getFileTypeByExtension(String extension){
        switch (extension.replace(".","").trim()){
            case (Extensions.EXE):
                return EXE;
            case (Extensions.JAR):
                return JAR;
            case (Extensions.JAVA):
                return JAVA;
            case (Extensions.CPP):
                return CPP;
            case (Extensions.JS):
                return JAVASCRIPT;
            case (Extensions.PY):
                return PYTHON;
            case (Extensions.CS):
                return CSHARP;
            default:
                return UNKNOWN;
        }
    }



    public static FileTypes getFileTypeByFilePath(Path filepath) {
        return getFileTypeByExtension(FilenameUtils.getExtension(filepath.getFileName().toString()));
    }

    public class Extensions{
        public final static String EXE = "exe";
        public final static String JAR = "jar";
        public final static String JAVA = "java";
        public final static String JS = "js";
        public final static String PY = "py";
        public final static String PAS = "pas";
        public final static String CPP = "cpp";
        public final static String CS = "cs";
    }


}
