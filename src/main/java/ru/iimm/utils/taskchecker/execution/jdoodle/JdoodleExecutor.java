package ru.iimm.utils.taskchecker.execution.jdoodle;

import com.sun.org.apache.xalan.internal.lib.Extensions;
import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.ApiResponse;
import io.swagger.client.api.CreditSpentApi;
import io.swagger.client.api.ExecuteApi;
import io.swagger.client.model.Credit;
import io.swagger.client.model.Execute;
import io.swagger.client.model.InlineResponse200;
import io.swagger.client.model.InlineResponse2001;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iimm.utils.taskchecker.FileTypes;
import ru.iimm.utils.taskchecker.execution.AbstractExecutor;

import java.util.Optional;
//import static ru.iimm.utils.taskchecker.FileTypes.Extensions.*;

public class JdoodleExecutor extends AbstractExecutor {
    
    private static final Logger log = LoggerFactory.getLogger( JdoodleExecutor.class );

    private static final String API_ENDPOINT = "https://api.jdoodle.com";

    /**
     * Client ID for subscription
     */
    private String clientId;

    /**
     * Client Secret for subscription
     */
    private String clientSecret;


    private ExecuteApi executeApi;

    private CreditSpentApi creditSpentApi;

    private ApiClient apiClient;

    public JdoodleExecutor(String clientId, String clientSecret) {
        this.clientId = clientId;
        this.clientSecret = clientSecret;
        executeApi = new ExecuteApi();
        creditSpentApi = new CreditSpentApi();
        apiClient = new ApiClient();
        apiClient.setBasePath(API_ENDPOINT);
        executeApi.setApiClient(apiClient);
        creditSpentApi.setApiClient(apiClient);
    }

//    public void init(){
//        apiClient.setBasePath(API_ENDPOINT);
//        executeApi.setApiClient(apiClient);
//        creditSpentApi.setApiClient(apiClient);
//    }

    @Override
    public Optional<String> execute(String script, String inputValue, String extension, String versionIndex) {
        Execute execute = new Execute();
        execute.setClientId(clientId);
        execute.setClientSecret(clientSecret);
        execute.setLanguage(getJdoodleLanguageTypeByExtension(extension));
        execute.setScript(script);
        execute.setStdin(inputValue);
        execute.setVersionIndex(versionIndex);

        try {
            log.info("=== Credit spend: [{}] (200 day limit of free jdoodle plan)",
                    getSpendCredits().orElseThrow(()-> new ApiException("Can't get spend credits")));
            log.info("=== Execute script: ===\n{}", script);
            ApiResponse<InlineResponse200> response2 =
                    executeApi.executePostWithHttpInfo(execute);
            String responseString = response2.getData().getOutput()!=null ?
                    response2.getData().getOutput().trim() : null;
            log.info("=== Output: [{}] ===", responseString);
            return Optional.ofNullable(responseString);
        } catch (ApiException e) {
            log.error("!!! Exception when execution: {}", e.getMessage());
            return Optional.empty();
//            throw new RuntimeException("Exception when execution: "+e.getMessage());
        }
    }


    public Optional<Integer> getSpendCredits(){
        Credit credit = new Credit(); // Credit | the body
        credit.setClientId(clientId);
        credit.setClientSecret(clientSecret);
        try {
            ApiResponse<InlineResponse2001> response2 =
                    creditSpentApi.creditSpentPostWithHttpInfo(credit);
            return Optional.ofNullable(response2.getData().getUsed());
        } catch (ApiException e) {
            log.error("!!! Exception when get spend credits: {}", e.getMessage());
            return Optional.empty();
        }
    }

        public static String getJdoodleLanguageTypeByExtension(String extension){
        extension = extension.replace(".","").trim();
        switch (extension){
            case (FileTypes.Extensions.JS):
                return JdoodleLanguages.NODEJS;
            case (FileTypes.Extensions.PY):
                return JdoodleLanguages.PY3;
            case (FileTypes.Extensions.CPP):
                return JdoodleLanguages.CPP17;
            case (FileTypes.Extensions.JAVA):
                return JdoodleLanguages.JAVA;
            case (FileTypes.Extensions.PAS):
                return JdoodleLanguages.PASCAL;
            case (FileTypes.Extensions.CS):
                return JdoodleLanguages.CSHARP;
            default:
                log.error("!!! JdoodleLanguageType is not defined for extension {}", extension);
                return JdoodleLanguages.UNKNOWN;
        }
    }


    /**
     * @see <a href=https://docs.jdoodle.com/compiler-api/compiler-api#what-languages-and-versions-supported>
     *     languages-and-versions-supported</>
     */
    class JdoodleLanguages{
        private final static String JAVA = "java";
        private final static String CPP = "cpp";
        private final static String CPP14 = "cpp14";
        private final static String CPP17 = "cpp17";
        private final static String PHP = "php";
        private final static String PY2 = "python2";
        private final static String PY3 = "python3";
        private final static String PASCAL = "pascal";
        private final static String NODEJS = "nodejs";
        private final static String CSHARP = "csharp";
        private final static String UNKNOWN = "unknown";

    }


}
