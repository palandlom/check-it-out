package ru.iimm.utils.taskchecker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.CodeSource;
import java.util.Optional;
import java.util.Properties;
import java.util.Random;

public class Utils {


    private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

    private static final String ARG_FORMAT_TTL = "--ttl";
    private static final String ARG_FORMAT_RDF = "--rdf";

    /**
     * Create a file from argument with index.
     *
     * @param args
     * @param argsFileIndex
     * @return
     */
    public static Optional<File> getArgAsFile(String args[], int argsFileIndex) {

        if (args[0] != null && (new File(args[0])).exists()
                && (new File(args[0])).isFile()) {
            return Optional.of(new File(args[0]));

        } else {
            return Optional.empty();
        }

    }

    public static Optional<URI> getUriFrom(String filePath) {
        try {
            Path path = Paths.get(filePath).normalize().toAbsolutePath();
            return Optional.of(path.toUri());
        } catch (InvalidPathException | NullPointerException ex) {
            LOGGER.error("Bad file path [{}]", filePath);
            return Optional.empty();
        }
    }


    public static Optional<File> getFileDirectory(File file) {
        if (file.exists()) {
            return Optional.of(file.getAbsoluteFile().getParentFile());
        } else {
            LOGGER.error("!!! File {} does not exist", file.getAbsolutePath());
            return Optional.empty();
        }
    }

    public static String readFileToString(File file) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(file));) {
            StringBuilder builder = new StringBuilder();
            String currentLine = reader.readLine();
            while (currentLine != null) {
                builder.append(currentLine);
                builder.append(System.lineSeparator());
                currentLine = reader.readLine();
            }
            return builder.toString();
        }
    }

    public static void writeStringToFile(String string, File file) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(string);
        }
    }

    ;

    /**
     * Add postfix to filename before extensions.
     *
     * @param file
     * @param postfix
     * @return
     */
    public static File insertPostfix(File file, String postfix) {
        StringBuilder bul = new StringBuilder(
                file.getAbsolutePath().toString());
        int pos = bul.lastIndexOf(".") > 0 ? bul.lastIndexOf(".")
                : bul.length();
        return new File(bul.insert(pos, postfix).toString());
    }

    public static File createFile(final String filePath, final long sizeInBytes) {
        File file = new File(filePath);
        try {
            file.createNewFile();
            RandomAccessFile raf = new RandomAccessFile(file, "rw");
            raf.setLength(sizeInBytes);
            raf.close();
        } catch (IOException e) {
            e.getMessage();
        }
        return file;
    }

    /**
     * Create dirs from given path.
     *
     * @param dirPath e.g. /somedir1/somedir2.
     */
    public static File forceCreateIfNotExists(String dirPath) {
        File dir = new File(dirPath);
        if (!dir.exists()) {
            dir.mkdirs();
        } else if (dir.isFile()) {
            dir.delete();
            dir.mkdirs();
        }
        return dir;
    }

    public static int getRandomInt(int upperRange) {
        Random random = new Random();
        return random.nextInt(upperRange);
    }

    public static String getRandomString(int targetStringLength) {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    public static Properties loadPropertiesOutsideJar(String properiesFileName) throws IOException {
        CodeSource src = Main.class.getProtectionDomain().getCodeSource();
        URL url = new URL(src.getLocation(), properiesFileName);
        Properties props = new Properties();
        InputStream resourceStream = new FileInputStream(url.getFile());
        props.load(resourceStream);
        return props;
    }



}