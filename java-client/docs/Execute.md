
# Execute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientId** | **String** |  |  [optional]
**clientSecret** | **String** |  |  [optional]
**script** | **String** |  |  [optional]
**language** | **String** |  |  [optional]
**versionIndex** | **String** |  |  [optional]



