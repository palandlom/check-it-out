using System;

namespace Expr1
{
    class Program
    {
         static double expr1(int a, int r)
        {
            var angleA = Math.Acos((double)a / (2 * r));
            var angleB = Math.PI / 2 - 2 * angleA;
            double sSector = 2 * r * r * angleB;
            double sTreug = 2 * r * r * Math.Sin(2 * angleA);
            return sSector + sTreug;
testerror
        }

        static void Main(string[] args)
        {
            var parts = Console.ReadLine().Split();
            var a = int.Parse(parts[0]);
            var r = int.Parse(parts[1]);
            Console.WriteLine(expr1(a, r));
        }
    }
}
