package ru.iimm.utils.taskchecker.execution;

import java.util.Optional;

public abstract class AbstractExecutor {


    /**
     * @param script       program to compile and execute
     * @param inputValue   Input value for script.
     * @param language     language of the script(refer the supported language list below) e.g. @see <a href=@see <a href=https://docs.jdoodle.com/compiler-api/compiler-api#what-languages-and-versions-supported</>
     * @param versionIndex version of the language to be used e.g. @see <a href=https://docs.jdoodle.com/compiler-api/compiler-api#what-languages-and-versions-supported</>
     * @return
     */
    public abstract Optional<String> execute(String script,
                                             String inputValue,
                                             String language,
                                             String versionIndex);


}
