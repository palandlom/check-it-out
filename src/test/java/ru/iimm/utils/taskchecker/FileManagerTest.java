package ru.iimm.utils.taskchecker;

import io.swagger.client.ApiClient;
import io.swagger.client.ApiException;
import io.swagger.client.ApiResponse;
import io.swagger.client.api.CreditSpentApi;
import io.swagger.client.api.ExecuteApi;
import io.swagger.client.model.Credit;
import io.swagger.client.model.Execute;
import io.swagger.client.model.InlineResponse200;
import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iimm.utils.taskchecker.execution.AbstractExecutor;
import ru.iimm.utils.taskchecker.execution.jdoodle.JdoodleExecutor;
import ru.iimm.utils.taskchecker.models.KeyRowDto;
import ru.iimm.utils.taskchecker.models.ResultRowDto;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static ru.iimm.utils.taskchecker.FileTypes.Extensions.*;

class FileManagerTest {
    public static final String RESULT_DIR_PATH = "./scripts";
    public static final String REPORT_FILE_PATH = RESULT_DIR_PATH+"/report_all.csv";
    public static final String KEYS_FILE_PATH = RESULT_DIR_PATH+"/keys.csv";
    public static final String AUTHOR_ID_A = "Ivanov";
    public static final String AUTHOR_ID_B = "Pertov";
    public static final String AUTHOR_ID_C = "Sidorov";

    private static final Logger log = LoggerFactory.getLogger( FileManagerTest.class );

    @BeforeEach
    void createDirAndFiles() throws IOException {
        FileUtils.deleteDirectory(new File(RESULT_DIR_PATH));
        File resultDir = Utils.forceCreateIfNotExists(RESULT_DIR_PATH);
        Utils.writeStringToFile(getScriptOnCpp(), new File(RESULT_DIR_PATH+String.format("/1_%s.%s",AUTHOR_ID_A, CPP)));
        Utils.writeStringToFile(getScriptOnPython(), new File(RESULT_DIR_PATH+String.format("/2_%s.%s",AUTHOR_ID_B, PY)));
        Utils.writeStringToFile(getScriptOnJava(), new File(RESULT_DIR_PATH+String.format("/3_%s.%s",AUTHOR_ID_C, JAVA)));
        Utils.writeStringToFile(getScriptOnCSharp(), new File(RESULT_DIR_PATH+String.format("/1_%s.%s",AUTHOR_ID_A, CS)));

//
//        Arrays.asList(
//                "1_file_"+AUTHOR_ID_A,
//                "2_file_"+AUTHOR_ID_B,
//                "3_file_"+AUTHOR_ID_C).forEach(num ->
//        Utils.createFile(resultDir.getAbsolutePath()+"/"+num+ FileTypes.WIN.fileSuffix(),20) );
    }

    @AfterEach
    void deleteDirAndFiles() throws IOException {

    }


    @Test
    void integralTest() throws IOException {
        // ==========================
        // === Read config ==========
        // ==========================
        // === Read config ===
//        String resourceName = "properties"; // could also be a constant
//        ClassLoader loader = Thread.currentThread().getContextClassLoader();
//        Properties props = new Properties();
//        try(InputStream resourceStream = loader.getResourceAsStream(resourceName)) {
//            props.load(resourceStream);
//            log.info(props.toString());
//        }
//        Assertions.assertFalse(props.isEmpty());

        // ==========================
        // === check getFilesPath ===
        // ==========================
        FileManager fileManager = new FileManager();
        List<Path> filesPaths = fileManager.getFilePaths(RESULT_DIR_PATH);
        Assertions.assertEquals(4,filesPaths.size());

        // ================================
        // === check save/read key file ===
        // ================================
        List<KeyRowDto> keys = new ArrayList<>();
        keys.add(new KeyRowDto("1","1", "1"));
        keys.add(new KeyRowDto("1","a a a", "a a a"));
        keys.add(new KeyRowDto("2","b b b", "b b b"));
        keys.add(new KeyRowDto("2","1 2 3", "1 2 3"));
        keys.add(new KeyRowDto("3","c c c", "c c c"));
//        keys.add(new KeyRowDto("1","in", "out"));
//        keys.add(new KeyRowDto("2","in", "out"));
//        keys.add(new KeyRowDto("3","in", "out"));

        fileManager.writeKeysFile(KEYS_FILE_PATH, keys);

        // ========================================
        // === check generation of empty report ===
        // ========================================
        InitialResultListCreator initialResultListCreator = new InitialResultListCreator();
        List<ResultRowDto> rows = initialResultListCreator.formInitialResultList(filesPaths);
        rows.stream().forEach(r -> log.info(r.toString()));
        Assertions.assertEquals(4, rows.size());

        // =============================
        // === Get and check output  ===
        // =============================
        String clientId = "75724477ff44b628b86ca0397bbba7bc";
        String clientSecret = "f15f1a667ffec71f313bd03cd2f72eab3007dd4e9d0993a07bdc4baeed59e823";
        String language = "python3";
        String versionIndex = "0";

        AbstractExecutor executor = new JdoodleExecutor(clientId, clientSecret);
        Checker checker = new Checker(executor);
        rows = checker.executeScriptsCheckResultAndFormFinalResultRows(keys,rows);

        // ==========================
        // === check write report ===
        // ==========================
        fileManager.writeResultFile(REPORT_FILE_PATH,rows);
        Assertions.assertEquals(7, fileManager.readResultFile(REPORT_FILE_PATH).size());
        Assertions.assertEquals(rows, fileManager.readResultFile(REPORT_FILE_PATH));
    }


    @Test
    void getFileAndFormInitialResultPaths() throws IOException {
        // === check getFilesPath ===
        FileManager fileManager = new FileManager();
        List<Path> filesPaths = fileManager.getFilePaths(RESULT_DIR_PATH);
        Assertions.assertEquals(4,filesPaths.size());

        // === check read key file ===
        List<KeyRowDto> keys = new ArrayList<>();
        keys.add(new KeyRowDto("1","in", "out"));
        keys.add(new KeyRowDto("2","in", "out"));
        keys.add(new KeyRowDto("3","in", "out"));
        fileManager.writeKeysFile(KEYS_FILE_PATH, keys);
        keys = fileManager.readKeysFile(KEYS_FILE_PATH);

        // === check generation of empty report ===
        InitialResultListCreator initialResultListCreator = new InitialResultListCreator();
        List<ResultRowDto> rows = initialResultListCreator.formInitialResultList(filesPaths);
        rows.stream().forEach(r -> log.info(r.toString()));
        Assertions.assertEquals(4, rows.size());

        // === Fill report ===
        String clientId = "75724477ff44b628b86ca0397bbba7bc";
        String clientSecret = "f15f1a667ffec71f313bd03cd2f72eab3007dd4e9d0993a07bdc4baeed59e823";
        String language = "python3";
        String versionIndex = "0";

        // ==========================
        // === check write report ===
        fileManager.writeResultFile(REPORT_FILE_PATH,rows);
        Assertions.assertEquals(4, fileManager.readResultFile(REPORT_FILE_PATH).size());
        Assertions.assertEquals(rows, fileManager.readResultFile(REPORT_FILE_PATH));

    }

    @Test
    public void apiTest(){
        String clientId = "e685250858106a8f96b4e6fa464449e1"; //Replace with your client ID

        String clientSecret = "c656454786a96c85143247033560b67551d582159177492f4613937b5e0e60b7"; //Replace with your client Secret

        clientId = "75724477ff44b628b86ca0397bbba7bc";
        clientSecret = "f15f1a667ffec71f313bd03cd2f72eab3007dd4e9d0993a07bdc4baeed59e823";
        String script = "";
        String language = "java";
        String versionIndex = "0";


        CreditSpentApi creditSpentApi = new CreditSpentApi();
        ApiClient client = new ApiClient();
        client.setBasePath("https://api.jdoodle.com");
        creditSpentApi.setApiClient(client);

        Credit credit = new Credit(); // Credit | the body
        credit.setClientId(clientId);
        credit.setClientSecret(clientSecret);

        ExecuteApi executeApi = new ExecuteApi();
        executeApi.setApiClient(client);

        try {
            ApiResponse response = creditSpentApi.creditSpentPostWithHttpInfo(credit);
            log.info("Response {}", response.getData());

            Execute execute = new Execute();
            execute.setClientId(clientId);
            execute.setClientSecret(clientSecret);
            execute.setLanguage("python3");
            execute.setScript("value = input(\"Please enter a string:\\n\")\n" +
                    " \n" +
                    "print(\"You entered\", {value})");
            execute.setStdin("pytho3-OK");
            execute.setVersionIndex("0");

            InlineResponse200 response2  = executeApi.executePost(execute);
            log.info("execute: {}", response2.toString());


            execute.setClientId(clientId);
            execute.setClientSecret(clientSecret);

            execute.setLanguage("java");
            execute.setScript("    import java.io.BufferedReader;\n" +
                    "    import java.io.IOException;\n" +
                    "    import java.io.InputStreamReader;\n" +
                    "    public class Test\n" +
                    "    {\n" +
                    "        public static void main(String[] args) throws IOException\n" +
                    "        {\n" +
                    "            BufferedReader reader =\n" +
                    "                       new BufferedReader(new InputStreamReader(System.in));\n" +
                    "            String name = reader.readLine();\n" +
                    "            System.out.println(name);       \n" +
                    "        }\n" +
                    "    }");
            execute.setStdin("java-OK");
            execute.setVersionIndex("1");

            response2  = executeApi.executePost(execute);
            log.info("execute: {}", response2.toString());

        } catch (ApiException e) {
            System.err.println("Exception when calling CreditSpentApi#creditSpentPost");
            e.printStackTrace();
        }
    }

    private String getScriptOnJava(){
        return "    import java.io.BufferedReader;\n" +
                "    import java.io.IOException;\n" +
                "    import java.io.InputStreamReader;\n" +
                "    public class Test\n" +
                "    {\n" +
                "        public static void main(String[] args) throws IOException\n" +
                "        {\n" +
                "            BufferedReader reader =\n" +
                "                       new BufferedReader(new InputStreamReader(System.in));\n" +
                "            String name = reader.readLine();\n" +
                "            System.out.println(name);       \n" +
                "        }\n" +
                "    }";
    }

    private String getScriptOnPython(){
        return "value = input() \n" +
                "print('{}'.format(value))\n";
    }

    private String getScriptOnCpp(){
        return "#include <iostream>\n" +
                "using namespace std;\n" +
                "\n" +
                "int main ()\n" +
                "{\n" +
                "    char input[100];\n" +
                "    cin.getline(input,sizeof(input));\n" +
                "  cout << input;\n" +
                "  return 0;\n" +
                "}";
    }

    private String getScriptOnCSharp(){
        return "using System;\n" +
                "using System.Collections.Generic;\n" +
                "\n" +
                "class Demo {\n" +
                "   static void Main() {\n" +
                "      string val;\n" +
                "      val = Console.ReadLine();\n" +
                "      Console.WriteLine(val);\n" +
                "   }\n" +
                "}";}


}