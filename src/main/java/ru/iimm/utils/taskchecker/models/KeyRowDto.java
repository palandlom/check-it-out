package ru.iimm.utils.taskchecker.models;

import com.opencsv.bean.CsvBindByName;

public class KeyRowDto {

    public KeyRowDto() {
    }

    public KeyRowDto(String taskId, String inputValue, String correctOutputValue) {
        this.taskId = taskId;
        this.inputValue = inputValue;
        this.correctOutputValue = correctOutputValue;
    }

    @CsvBindByName(column = "01_TaskID")
    //@CsvBindByPosition(position = 4)
    private String taskId;

    @CsvBindByName(column = "02_Input-value")
    //@CsvBindByPosition(position = 3)
    private String inputValue;

    @CsvBindByName(column = "03_Correct-out-value")
    //@CsvBindByPosition(position = 5)
    private String correctOutputValue;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    public String getCorrectOutputValue() {
        return correctOutputValue;
    }

    public void setCorrectOutputValue(String correctOutputValue) {
        this.correctOutputValue = correctOutputValue;
    }

    @Override
    public String toString() {
        return "KeyRowDto{" +
                "taskId='" + taskId + '\'' +
                ", inputValue='" + inputValue + '\'' +
                ", correctOutputValue='" + correctOutputValue + '\'' +
                '}';
    }
}
