package ru.iimm.utils.taskchecker.models;

import com.opencsv.bean.CsvBindByName;

import java.util.Objects;

public class ResultRowDto {

    public ResultRowDto() {
    }

    public ResultRowDto(ResultRowDto resultRowDto) {
        this.taskId = resultRowDto.taskId;
        this.unitFilePath = resultRowDto.unitFilePath;
        this.inputValue = resultRowDto.inputValue;
        this.outputValue = resultRowDto.outputValue;
        this.correctOutputValue = resultRowDto.correctOutputValue;
        this.result = resultRowDto.result;
        this.authorId = resultRowDto.authorId;
        this.fileExtension = resultRowDto.fileExtension;
        this.language = resultRowDto.language;
        this.versionIndex = resultRowDto.versionIndex;
    }

    @CsvBindByName(column = "01_TaskID")
    //@CsvBindByPosition(position = 4)
    private String taskId;

    @CsvBindByName(column = "02_Solution-filepath")
    //@CsvBindByPosition(position = 1)
    private String unitFilePath;

    @CsvBindByName(column = "03_Input-value")
    //@CsvBindByPosition(position = 3)
    private String inputValue;

    @CsvBindByName(column = "04_Output-value")
    //@CsvBindByPosition(position = 4)
    private String outputValue;

    @CsvBindByName(column = "05_Correct-out-value")
    //@CsvBindByPosition(position = 5)
    private String correctOutputValue;

    @CsvBindByName(column = "06_Result")
    //@CsvBindByPosition(position = 2)
    private String result;

    @CsvBindByName(column = "07_Author-ID")
    //@CsvBindByPosition(position = 0)
    private String authorId;

    @CsvBindByName(column = "08_Solution-filetype")
    //@CsvBindByPosition(position = 6)
    private String fileExtension;

    @CsvBindByName(column = "09_Solution-language")
    //@CsvBindByPosition(position = 6)
    private String language;

    @CsvBindByName(column = "10_Solution-language-version-index")
    //@CsvBindByPosition(position = 6)
    private String versionIndex;


    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    public String getOutputValue() {
        return outputValue;
    }

    public void setOutputValue(String outputValue) {
        this.outputValue = outputValue;
    }

    public String getCorrectOutputValue() {
        return correctOutputValue;
    }

    public void setCorrectOutputValue(String correctOutputValue) {
        this.correctOutputValue = correctOutputValue;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getUnitFilePath() {
        return unitFilePath;
    }

    public void setUnitFilePath(String unitFilePath) {
        this.unitFilePath = unitFilePath;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    public void setFileExtension(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getVersionIndex() {
        return versionIndex;
    }

    public void setVersionIndex(String versionIndex) {
        this.versionIndex = versionIndex;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResultRowDto that = (ResultRowDto) o;
        return Objects.equals(taskId, that.taskId) &&
                Objects.equals(unitFilePath, that.unitFilePath) &&
                Objects.equals(inputValue, that.inputValue) &&
                Objects.equals(outputValue, that.outputValue) &&
                Objects.equals(correctOutputValue, that.correctOutputValue) &&
                Objects.equals(result, that.result) &&
                Objects.equals(authorId, that.authorId) &&
                Objects.equals(fileExtension, that.fileExtension) &&
                Objects.equals(language, that.language) &&
                Objects.equals(versionIndex, that.versionIndex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(taskId, unitFilePath, inputValue, outputValue, correctOutputValue, result, authorId, fileExtension, language, versionIndex);
    }

    @Override
    public String toString() {
        return "ResultRowDto{" +
                "taskId='" + taskId + '\'' +
                ", unitFilePath='" + unitFilePath + '\'' +
                ", inputValue='" + inputValue + '\'' +
                ", outputValue='" + outputValue + '\'' +
                ", correctOutputValue='" + correctOutputValue + '\'' +
                ", result='" + result + '\'' +
                ", authorId='" + authorId + '\'' +
                ", fileExtension='" + fileExtension + '\'' +
                ", language='" + language + '\'' +
                ", versionIndex='" + versionIndex + '\'' +
                '}';
    }
}
