arr = input().split()
#получаем сумму первых 2 чисел
max_sum = int(arr[0]) + int(arr[1])
summers = []
summers.append(max_sum)
i = 0
#ищем максимальную сумму в последовательности
for i in range(len(arr)):
    if i >= 2:
        max_sum += int(arr[i])
        summers.append(max_sum)
        if max_sum < 0:
            max_sum = 0
    i += 1

if max_sum == 0:
    print(max_sum)
else:
    print(max(summers))