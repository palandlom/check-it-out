# check-it-out
For checking [jdoodle-api](https://www.jdoodle.com/compiler-api/) is using. You should have cliendId and clientSecret 
which can be obtained for free from [jdoodle-api](https://www.jdoodle.com/compiler-api/) by using FreePlan (limitation 200 checking per day).

# build
To build you need `maven` and `java ver. >=8`.
Run `build.sh` to get jar-file in `./bin` directory.

# run
To run `checkitout` execute:
```bash
    java -jar ./checkitout.jar
```
Note: `properties` file should be in the same dir as `checkitout.jar`

# configuration
Configuration of `check-it-out` is in `properties` file containing the following options:
```properties
# Jdoodle-api id and secret
clientId=e685250858106a8f96b4e6fa464449e1
clientSecret=ab84aa2c0386a04ce4708fdee6db47c932bfa75df3b2d964bad95035dad52ba

# Path to scripts (aka files with source code of task solutions to check)
resultsDirPath=./scripts

# Right output for tasks
keysFilePath=./keys.csv

# Final report
resultsFilePath=./results.csv

# Errors happened during compilations of scripts by jdoodle
errorsFilePath=./errors.log
```
Use `./bin/properties` file as a default config-file.

References:
* jdoodle-api: https://www.jdoodle.com/compiler-api/
* check-it-out soundtrack - The Rockafeller Skank by Fatboy Slim: https://youtu.be/Zj5JmJMSLCw?t=9 