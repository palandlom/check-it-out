package ru.iimm.utils.taskchecker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iimm.utils.taskchecker.execution.AbstractExecutor;
import ru.iimm.utils.taskchecker.execution.jdoodle.JdoodleExecutor;
import ru.iimm.utils.taskchecker.models.KeyRowDto;
import ru.iimm.utils.taskchecker.models.ResultRowDto;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

public class Main {
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        log.info("=== Start checking ===");
        // ========================
        // === Read config file ===
        // ========================
        Properties props = Utils.loadPropertiesOutsideJar("properties");
//        log.info(props.toString());
        String resultsDir = props.getProperty("resultsDirPath");
        String resultsFilePath = props.getProperty("resultsFilePath");
        String clientId = props.getProperty("clientId");
        String clientSecret = props.getProperty("clientSecret");
        String keysFilePath = props.getProperty("keysFilePath");
        String errorsFilePath=props.getProperty("errorsFilePath") == null ? "./errors.log" : props.getProperty("errorsFilePath");
        Utils.forceCreateIfNotExists(resultsDir);

        // ====================================================
        // === Collect scripts and form initial result file ===
        // ====================================================
        FileManager fileManager = new FileManager();
        List<Path> filesPaths = fileManager.getFilePaths(resultsDir);
        log.info("=== Get [{}] script-files from [{}]:", filesPaths.size(), resultsDir);
        InitialResultListCreator initialResultListCreator = new InitialResultListCreator();
        List<ResultRowDto> initialResRows = initialResultListCreator.formInitialResultList(filesPaths);
        initialResRows.stream().forEach(r -> log.info("Path:[{}] Author:[{}] File-ext.:[{}] TaskID:[{}]  ",
                r.getUnitFilePath(),r.getAuthorId(),r.getFileExtension(),r.getTaskId()));


        // ===========================================
        // === Check results and write result file ===
        // ===========================================
        AbstractExecutor executor = new JdoodleExecutor(clientId, clientSecret);
        List<KeyRowDto> keys = fileManager.readKeysFile(keysFilePath);
        log.info("=== Get [{}] keys from [{}]:", keys.size(), Paths.get(keysFilePath).toAbsolutePath().normalize());
        if (keys.isEmpty()) {fileManager.writeKeysFileExample(keysFilePath);}

        Checker checker = new Checker(executor);
        log.info("=== Start online checking ===");
        List<ResultRowDto> finalResultRows = checker.executeScriptsCheckResultAndFormFinalResultRows(keys,initialResRows);
        log.info("=== End online checking ===");
        // ====================
        // === Write report ===
        // ====================
        fileManager.writeResultFile(resultsFilePath,finalResultRows);
        fileManager.writeErrorFile(checker.getErrors(),errorsFilePath);
        log.info("Final report has been saved to: [{}]", Paths.get(resultsFilePath).toAbsolutePath().normalize());
    }
}
