package ru.iimm.utils.taskchecker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class APITest {
    public static void main(String args[]) {

        String clientId = "75724477ff44b628b86ca0397bbba7bc"; //Replace with your client ID
        String clientSecret = "f15f1a667ffec71f313bd03cd2f72eab3007dd4e9d0993a07bdc4baeed59e823"; //Replace with your client Secret
        String script = "";
        String language = "java";
        String versionIndex = "0";

        try {
            URL url = new URL("https://api.jdoodle.com/v1/execute");
            url = new URL("https://api.jdoodle.com/v1/credit-spent");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");

            String input = "{\"clientId\": \"" + clientId + "\",\"clientSecret\":\"" + clientSecret + "\"} ";
//            String input = "{\"clientId\": \"" + clientId + "\",\"clientSecret\":\"" + clientSecret + "\",\"script\":\"" + script +
//                    "\",\"language\":\"" + language + "\",\"versionIndex\":\"" + versionIndex + "\"} ";
//            input = "public class MyClass {\n" +
//                    "    public static void main(String args[]) {\n" +
//                    "      int x=10;\n" +
//                    "      int y=25;\n" +
//                    "      int z=x+y;\n" +
//                    "\n" +
//                    "      System.out.println(\"Sum of x+y = \" + z);\n" +
//                    "    }\n" +
//                    "}";
//
            System.out.println(input);

            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(input.getBytes());
            outputStream.flush();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new RuntimeException("Please check your inputs : HTTP error code : "+ connection.getResponseCode());
            }

            BufferedReader bufferedReader;
            bufferedReader = new BufferedReader(new InputStreamReader(
                    (connection.getInputStream())));

            String output;
            System.out.println("Output from JDoodle .... \n");
            while ((output = bufferedReader.readLine()) != null) {
                System.out.println(output);
            }

            connection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}