package ru.iimm.utils.taskchecker.models;

import java.util.Objects;

public class ErrorDto {

    private String message;
    private String unitFilePath;
    private String authorId;
    private String taskId;
    private String inputValue;


    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUnitFilePath() {
        return unitFilePath;
    }

    public void setUnitFilePath(String unitFilePath) {
        this.unitFilePath = unitFilePath;
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getId(){
        return String.valueOf(Math.abs(Objects.hash(authorId, taskId, message, unitFilePath, inputValue)));
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer();
        sb.append("=======================================================================");
        sb.append("\nError ID:\t").append(getId());
        sb.append("\nAuthorId:\t").append(authorId);
        sb.append("\nTaskId:\t\t").append(taskId);
        sb.append("\nInputValue:\t\t").append(inputValue);
        sb.append("\nUnitFilePath:\t").append(unitFilePath);
        sb.append("\n--- Error message ---\n").append(message);
        sb.append("\n=======================================================================");
        return sb.toString();
    }
}
