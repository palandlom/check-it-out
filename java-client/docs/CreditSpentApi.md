# CreditSpentApi

All URIs are relative to *https://api.jdoodle.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**creditSpentPost**](CreditSpentApi.md#creditSpentPost) | **POST** /credit-spent | Credit Spent


<a name="creditSpentPost"></a>
# **creditSpentPost**
> creditSpentPost(credit)

Credit Spent

Endpoint to execute code

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.CreditSpentApi;


CreditSpentApi apiInstance = new CreditSpentApi();
Credit credit = new Credit(); // Credit | the body
try {
    apiInstance.creditSpentPost(credit);
} catch (ApiException e) {
    System.err.println("Exception when calling CreditSpentApi#creditSpentPost");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credit** | [**Credit**](Credit.md)| the body |

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

