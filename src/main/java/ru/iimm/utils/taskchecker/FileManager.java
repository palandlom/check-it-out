package ru.iimm.utils.taskchecker;

import com.opencsv.CSVReader;
import com.opencsv.bean.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.iimm.utils.taskchecker.models.ErrorDto;
import ru.iimm.utils.taskchecker.models.KeyRowDto;
import ru.iimm.utils.taskchecker.models.ResultRowDto;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileManager {

    private static final Logger log = LoggerFactory.getLogger(FileManager.class);

    public List<Path> getFilePaths(String dirPath) throws IOException {
        File dir = new File(dirPath);
        if (dir.exists() && dir.isDirectory()) {
            try (Stream<Path> paths = Files.walk(Paths.get(dir.toURI()))) {
                return paths.filter(Files::isRegularFile)
                        .peek(f -> log.info("... Found file [{}]", f.getFileName()))
//                        .filter(f->extension != null && extension.length()>0 ? f.getFileName().toString().endsWith(extension) : true)
                        .peek(f -> log.info("... Add file for checking [{}]", f.getFileName()))
                        .collect(Collectors.toList());
            }
        } else {
            throw new IOException(String.format("Dir [%s] can't be read", dirPath));
        }
    }

    /**
     * http://zetcode.com/articles/opencsv/
     *
     * @param filePath
     * @return
     * @throws IOException
     */
    public List<ResultRowDto> readResultFile(String filePath) throws IOException {
        List<ResultRowDto> resultRows = new ArrayList<>();
        try {
            CsvToBean<ResultRowDto> csv = new CsvToBean();
            CSVReader csvReader = new CSVReader(new FileReader(filePath));
            csv.setCsvReader(csvReader);

            HeaderColumnNameMappingStrategy<ResultRowDto> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(ResultRowDto.class);
//            ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
            csv.setMappingStrategy(strategy);

            return csv.parse().stream()
                    .peek(r -> log.info(String.format("... read row: %s", r.toString())))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        }
    }

    public List<KeyRowDto> readKeysFile(String filePath) throws IOException {
        try {
            log.info("=== Read keys file ===");
            CsvToBean<KeyRowDto> csv = new CsvToBean();
            CSVReader csvReader = new CSVReader(new FileReader(filePath));
            csv.setCsvReader(csvReader);

            HeaderColumnNameMappingStrategy<KeyRowDto> strategy
                    = new HeaderColumnNameMappingStrategy<>();
            strategy.setType(KeyRowDto.class);
//            ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
            csv.setMappingStrategy(strategy);

            return csv.parse().stream()
                    .peek(r -> log.info(String.format("... read key-row: %s", r.toString())))
                    .collect(Collectors.toList());
        } catch (Exception e) {
            log.error("Can't read keys file {} \n - return empty keys list", filePath);
            return Collections.emptyList();
        }
    }


    @SuppressWarnings({"rawtypes", "unchecked"})
    @Deprecated //not used
    private static ColumnPositionMappingStrategy setColumMapping() {
        ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
        strategy.setType(ResultRowDto.class);
        String[] columns = new String[]{"id", "firstName", "lastName", "country", "age"};
        strategy.setColumnMapping(columns);
        return strategy;
    }

    public void writeResultFile(String filePath, List<ResultRowDto> resultRows) throws IOException {
        try {
            Writer writer = new FileWriter(filePath);
            StatefulBeanToCsvBuilder<ResultRowDto> builder = new StatefulBeanToCsvBuilder<>(writer);
            StatefulBeanToCsv<ResultRowDto> beanWriter = builder.build();
            beanWriter.write(resultRows);
            writer.close();
        } catch (Exception e) {
            throw new IOException(String.format("Error during write file [%s] - [%s]", filePath, e.getMessage()));
        }
    }

    public void writeKeysFile(String filePath, List<KeyRowDto> resultRows) throws IOException {
        try {
            Writer writer = new FileWriter(filePath);
            StatefulBeanToCsvBuilder<KeyRowDto> builder = new StatefulBeanToCsvBuilder<>(writer);
            StatefulBeanToCsv<KeyRowDto> beanWriter = builder.build();
            beanWriter.write(resultRows);
            writer.close();
        } catch (Exception e) {
            throw new IOException(String.format("Error during write file [%s] - [%s]", filePath, e.getMessage()));
        }
    }

    public void writeKeysFileExample(String filePath) throws IOException {
        writeKeysFile(filePath, Collections.singletonList(new KeyRowDto()));
    }

    public String writeErrorFile(List<ErrorDto> errors, String filePath) {
        errors.sort(Comparator.comparing(e -> e.getAuthorId()));
        Path outFilePath = Paths.get(filePath);
        try {
            Files.write(outFilePath, errors.stream().map(e -> e.toString()).collect(Collectors.toList()),
                    Charset.defaultCharset());
        } catch (IOException e) {
            throw new RuntimeException("IOException with: "+ e.getMessage());
        }
        log.info("Errors are save to [{}]", outFilePath.toAbsolutePath().normalize().toString());
        return outFilePath.toAbsolutePath().normalize().toString();
    }


}
